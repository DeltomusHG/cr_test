from django.db import models

class Store(models.Model):
    name = models.CharField(max_length=100)

    slug = models.SlugField(max_length=100)

    created = models.TimeField(auto_now_add=True)


class StoreSchedule(models.Model):
    weekday = models.CharField(max_length=50)

    opens = models.TimeField(blank=False, null=False)

    closes = models.TimeField(blank=False, null=False)

    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['weekday','store']