from django.db import models
from django.contrib.postgres.fields import ArrayField
from pink_basket.stores.models import Store

class Orders(models.Model):
    order_number = models.IntegerField(blank=False, null=False)

    products = ArrayField(models.CharField(max_length=100))

    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    delivery_date = models.DateTimeField()
    
    total_amount = models.FloatField()

