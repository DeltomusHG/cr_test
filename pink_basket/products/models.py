from django.db import models
from pink_basket.stores.models import Store

class Product(models.Model):
    name = models.CharField(max_length=100)

    slug = models.SlugField(max_length=100)

    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    price = models.FloatField(blank=False, null=False)

    elaboration_time = models.IntegerField()


